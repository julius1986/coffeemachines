package home;

public class NotEnoughMoneyException extends RuntimeException {
    public NotEnoughMoneyException(CoffeeType orderedCoffeeType) {
        super("Not enough money to prepare " + orderedCoffeeType.name());
    }
}
