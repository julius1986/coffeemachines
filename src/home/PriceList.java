package home;

import java.util.HashMap;
import java.util.Map;

public class PriceList {

    private Map<CoffeeType, Integer> prices = new HashMap<CoffeeType, Integer>();

    public void addPrice(CoffeeType coffeeType, int price) {
        prices.put(coffeeType, new Integer(price));
    }

    public int getPrice(CoffeeType coffeeType) {
        return prices.get(coffeeType);
    }
}
