package home;


public class NotEnoughCoffeeException extends RuntimeException {
    public NotEnoughCoffeeException(CoffeeType orderedCoffeeType) {
        super("Not enough portion of coffee to prepare " + orderedCoffeeType.name());
    }
}
