package home;

public class MoneyAcceptor {

    private MoneyStorage moneyStorage;

    public MoneyAcceptor(MoneyStorage moneyStorage) {
        this.moneyStorage = moneyStorage;
    }

    public void acceptMoney(int amountOfMoney) {
        System.out.println("You have put next sum of money: " + amountOfMoney);
        moneyStorage.add(amountOfMoney);
    }

    public void compareIncomingMoneyWithPrice() {
    moneyStorage.getLastTransaction();

    }
}
