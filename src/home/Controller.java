package home;

public class Controller {

    private CoffeeType orderedCoffeeType;
    private PriceList priceList;
    private CoffeeStorage coffeeStorage;
    private MoneyStorage moneyStorage;

    public void setPriceList(PriceList priceList) {
        this.priceList = priceList;
    }

    public PriceList getPriceList() {
        return priceList;
    }

    public void setCoffeeStorage(CoffeeStorage coffeeStorage) {
        this.coffeeStorage = coffeeStorage;
    }

    public CoffeeStorage getCoffeeStorage() {
        return coffeeStorage;
    }

    public void setMoneyStorage(MoneyStorage moneyStorage) {
        this.moneyStorage = moneyStorage;
    }

    public MoneyStorage getMoneyStorage() {
        return moneyStorage;
    }

    public void acceptOrderFromInterface(CoffeeType orderedCoffeeType) {
        this.orderedCoffeeType = orderedCoffeeType;
    }

    public void validateAcceptedMoney() throws RuntimeException {
        int lastTransaction = this.moneyStorage.getLastTransaction();
        int priceOfCoffeeType = this.priceList.getPrice(orderedCoffeeType);
        if(lastTransaction < priceOfCoffeeType) {
            throw new NotEnoughMoneyException(orderedCoffeeType);
        }
    }

    public void acceptMoney(int money) {
        this.moneyStorage.add(money);
    }

    public void validateCoffeeAmount() {
        int amountOfCoffee = this.coffeeStorage.getAmountOfCoffee(orderedCoffeeType);
        if(amountOfCoffee < 1) {
            throw new NotEnoughCoffeeException(orderedCoffeeType);
        }
    }

    public void returnCoffee() {
        coffeeStorage.removeCoffeePortion(orderedCoffeeType);

    }

    public void returnMoney() {
        int lastTransaction = this.moneyStorage.getLastTransaction();
        this.moneyStorage.removeMoney(lastTransaction);
    }
}
