package home;

public class MoneyStorage {

    private int bottomLessMoneyBucket;
    private int lastTransaction;

    public MoneyStorage(int initialAmountOfMoney) {
        bottomLessMoneyBucket += initialAmountOfMoney;
    }

    public void add(int amountOfMoney) {
        bottomLessMoneyBucket += amountOfMoney;
        lastTransaction = amountOfMoney;
    }

    public int getLastTransaction() {
        return lastTransaction;
    }

    public void removeMoney(int lastTransaction) {
        bottomLessMoneyBucket -= lastTransaction;
        System.out.println("Money backed ");
    }
}
