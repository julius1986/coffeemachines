package home;

import java.util.HashMap;
import java.util.Map;

public class CoffeeStorage {

    private Map<CoffeeType, Integer> portions = new HashMap<CoffeeType, Integer>();

    public void addCoffeePortions(CoffeeType coffeeType, int numberOfPortions) {
        portions.put(coffeeType, new Integer(numberOfPortions));
    }

    public int getAmountOfCoffee(CoffeeType orderedCoffeeType) {
        return portions.get(orderedCoffeeType);
    }

    public void removeCoffeePortion(CoffeeType orderedCoffeeType) {
        portions.put(orderedCoffeeType, portions.get(orderedCoffeeType) - 1);
    }
}
