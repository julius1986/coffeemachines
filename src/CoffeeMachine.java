import home.*;

import static home.CoffeeType.*;


public class CoffeeMachine {

    public static void main(String[] args) {
        UserInterFaceController userInterFaceController = new UserInterFaceController();

        PriceList priceList = new PriceList();
        priceList.addPrice(AMERICANO, 5);
        priceList.addPrice(EXPRESSO, 10);
        priceList.addPrice(LATE, 15);
        CoffeeStorage coffeeStorage = new CoffeeStorage();
        coffeeStorage.addCoffeePortions(AMERICANO, 30);
        coffeeStorage.addCoffeePortions(EXPRESSO, 20);
        coffeeStorage.addCoffeePortions(LATE, 15);
        MoneyStorage moneyStorage = new MoneyStorage(100);


        // Controller setup
        Controller controller = new Controller();
        controller.setPriceList(priceList);
        controller.setCoffeeStorage(coffeeStorage);
        controller.setMoneyStorage(moneyStorage);



        // Consumer actions
        controller.acceptMoney(15);
        controller.acceptOrderFromInterface(userInterFaceController.pushButton(LATE));
        try{
            controller.validateAcceptedMoney();
            controller.validateCoffeeAmount();
        } catch (NotEnoughCoffeeException ex){
            controller.returnMoney();//todo: implement
        } catch (NotEnoughMoneyException ex){
            controller.returnMoney();//todo: implement
        }
        controller.returnCoffee();//todo: implement
    }


}
